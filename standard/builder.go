package standard

import (
	"context"
)

// builder标准
type IBuilder interface {
	// context
	WithContext(ctx context.Context) IBuilder
	// tableName
	TableName(tableName string) IBuilder
	Table(table ITable) IBuilder

	// 插入
	Insert(value interface{}) error
	// 更新
	Update(attrs ...interface{}) error
	// 删除
	Delete() error
	// 查询
	Select(value ...string) IBuilder
	// 查询单条
	Find(value interface{}) error
	// 查询多条
	Get(values interface{}) error
	// 统计
	Count(value interface{}) error

	// 查询且条件
	Where(query interface{}, args ...interface{}) IBuilder
	// 查询定位
	Offset(offset interface{}) IBuilder
	// 查询区间
	Limit(limit interface{}) IBuilder
	// 排序
	Order(reorder ...interface{}) IBuilder

	// 事务开始
	Begin(ctx context.Context) (IBuilder, error)
	// 事务回滚
	Rollback() error
	// 事务提交
	Commit() error
}
