package standard

import (
	"context"
	"database/sql"
)

// IQuery标准
type IQuery interface {
	// DB
	DB() DB
	// Clear
	Clear()
	// 组装数据
	Raw(sql string, values ...interface{}) IQuery
	// 执行
	Exec() (result sql.Result, err error)
	// 单条查询
	Query(ctx context.Context, dest ...interface{}) error
	// 单条查询
	Find(ctx context.Context, dest interface{}) error
	// 多条查询
	Get(ctx context.Context, dest interface{}) error
	// Begin
	Begin(ctx context.Context) error
	// Commit
	Commit() error
	// Rollback
	Rollback() error
}
