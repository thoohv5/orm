package standard

// 模型字段属性
type Field struct {
	Name string
	Type string
	Tag  string
}

type ISchema interface {
	GetTableName() string
	GetField(name string) *Field
	GetFieldKeys() []string
	GetFieldValues() []interface{}
	Model() IModel
	Parse(m IModel) ISchema
	Values(d interface{}) []interface{}
}
