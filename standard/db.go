package standard

import (
	"context"
	"database/sql"
)

// DB is a minimal function set of db
type DB interface {
	Query(query string, args ...interface{}) (*sql.Rows, error)
	QueryContext(ctx context.Context, query string, args ...interface{}) (*sql.Rows, error)
	QueryRow(query string, args ...interface{}) *sql.Row
	QueryRowContext(ctx context.Context, query string, args ...interface{}) *sql.Row
	Exec(query string, args ...interface{}) (sql.Result, error)
}

var _ DB = (*sql.DB)(nil)
var _ DB = (*sql.Tx)(nil)
