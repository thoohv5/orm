package standard

type SqlType int

type IGrammar interface {
	Set(name SqlType, vars ...interface{})
	Build(orders ...SqlType) (string, []interface{})
}
