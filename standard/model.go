package standard

import (
	"context"
)

// 数据Model标准
type IModel interface {
	IConnection
	ITable
	IFilter
	// 添加
	Add(ctx context.Context, value ITable) error
	// 更新
	Update(ctx context.Context, param IModelQuery, update map[string]interface{}) error
	// 删除
	Delete(ctx context.Context, param IModelQuery) error
	// 查询单条
	Find(ctx context.Context, param IModelQuery, result ITable) error
	// 查询多条
	Get(ctx context.Context, param IModelQuery, result interface{}) error
	// GetBuilder
	GetBuilder() IBuilder
	// SetBuilder
	SetBuilder(build IBuilder) IModel
}

// 数据库表标准
type ITable interface {
	TableName() string
}

type DefaultTable struct {
	tableName string
}

func NewDefaultTable(tableName string) ITable {
	return &DefaultTable{
		tableName: tableName,
	}
}
func (dt *DefaultTable) TableName() string {
	return dt.tableName
}

// 数据库链接标准
type IConnection interface {
	Connection() string
}

// 公共查询
type IModelQuery interface {
	GetCommonReq() *CommonReq
}

type CommonReq struct {
	Start int32 `json:"start,omitempty"`
	Limit int32 `json:"limit,omitempty"`
	// 排序：sort=otc_type,-created_at,*custom
	// 以符号开头，可选符号：(+或空 正序）（- 倒序）（* 自定义复杂排序标识关键词）
	Sorts []string `json:"sorts,omitempty"`
}

// 数据库过滤标准
type IFilter interface {
	BuildFilterQuery(build IBuilder, filter IModelQuery) IBuilder
}
