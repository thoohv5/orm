package standard

import (
	"database/sql"
	"time"
)

type IConnect interface {
	Name() string
	Driver() string
	DB() *sql.DB
}

type IConfig interface {
	Name() string
	Driver() string
	DNS() string
	MaxIdle() int
	MaxOpen() int
	MaxLifetime() time.Duration
}
