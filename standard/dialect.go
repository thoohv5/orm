package standard

// 本地化
type IDialect interface {
	// 是否存在表
	IsExistTable(tableName string) (string, []interface{})
}
