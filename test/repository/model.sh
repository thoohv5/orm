#!/bin/bash

# 数据库配置
host=mysql
port=3306
user=root
passwd=password

#  databases数据库名
databases=(test)
#  放置数据库名-表名
test="aa"

readonly module_name="$(awk 'NR==1{print $2}' ../go.mod)"

exist_command() {
  if [ "$1" = '' ]; then
    return 0
  fi

  printf "  command \`%s\`: " "$1"
  if [ "$(command -v "$1")" != '' ]; then
    printf "\E[32m[EXIST]\E[0m \n"
    return 0
  else
    printf "\E[31m[MISS]\E[0m \n"
    printf "  - Please install it, via %s \n" "$2"
    break=1
    return 1
  fi
}

check_command() {
  printf "[1/4] check command:\n"
  break=0
  exist_command db2struct "\`go get github.com/thoohv5/converter/cmd/converter\`"
  if [ $break = 1 ]; then
    exit
  fi
}

sql2struct() {

  database=$1
  target=$2

  printf "[4/4] exec sql2struct:\n"

  tables=$(eval echo '${'"$database"'[@]}')

  converter -dsn="$user:$passwd@tcp($host:$port)/$database?charset=utf8" -tables "$tables" -pkg "$package" -path="$target" -jsonTag >/dev/null 2>&1

  grep -rl 'struct' "$target" --exclude-dir=dao --exclude="base.go" | xargs sed -i "" "s/struct {/&\n\tBase \`db:\"-\"\`/g"

  printf "finish\n"
}

clear() {
  printf "[2/4] clear cache: "
  rm -rf "$target"/*.go
  printf "finish\n"
}

base() {
  cat >"$target/base.go" <<EOF
package ${database//_/-}

type Base struct {
}

func (*Base) Connection() string {
	return "${database//_/-}"
}
EOF
}

dao() {

  target=$1
  table=$2

  if [ ! -d "$target/dao" ]; then
    mkdir "$target/dao"
  fi

  if [ ! -f "$target/dao/$table.go" ]; then
    cpTable=$(camel "$table")
    cat >"$target/dao/$table.go" <<EOF
package dao

import (
	"fmt"
	"sync"

	"gitee.com/thoohv5/orm/model"
	"gitee.com/thoohv5/orm/standard"
	"$module_name/repository/test"
)

type ${cpTable} struct {
	*model.Model
	test.${cpTable}
}

type ${cpTable}Condition struct {
	model.Condition

  // 条件
}

var (
	__once${cpTable} sync.Once
	m${cpTable}      *${cpTable}
)

type I${cpTable} interface {
	standard.IModel
}

func New${cpTable}() I${cpTable} {
	__once${cpTable}.Do(func() {
		m${cpTable} = new(${cpTable})
		m${cpTable}.Model = model.RegisterModel(m${cpTable})
	})
	return m${cpTable}
}

func (a *${cpTable}) BuildFilterQuery(build standard.IBuilder, filter standard.IModelQuery) standard.IBuilder {
	condition := filter.(*${cpTable}Condition)

	// 查询
	fmt.Println(condition)

	return build
}
EOF
  fi
}

camel() {
  PARA=$1

  arr=$(echo "$PARA" | tr '_' ' ')
  result=''
  for var in ${arr[@]}; do
    firstLetter=$(echo "${var:0:1}" | awk '{print toupper($0)}')
    otherLetter=${var:1}
    result=$result$firstLetter$otherLetter
  done

  firstResult=$(echo "${result:0:1}" | tr "[a-z]" "[A-Z]")
  result=$firstResult${result:1}
  echo "$result"
}

handle() {
  check_command

  for database in "${databases[@]}"; do
    # 生成的package
    package="$database"
    # 生成的文件全路径, 不带文件名称
    target=${database//_/-}
    #    target=${database}

    if [ ! -d "$target" ]; then
      mkdir "$target"
    fi

    clear
    base

    # 生成model
    sql2struct "$database" "$target"

    # shellcheck disable=SC2045
    for var in `ls $target`; do
      if [ ! -d "$target/$var" ]; then
        if [[ "$var" != base* ]]; then
          dao "$target" $(basename "$var" ".go")
        fi
      fi
    done

    gofmt -l -w -s "$target"

  done
}

handle
printf "\E[32m[SUCCESS]\E[0m done!\n"
