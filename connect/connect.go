package connect

import (
	"database/sql"

	"gitee.com/thoohv5/orm/logger"
	"gitee.com/thoohv5/orm/standard"
)

type connect struct {
	dial   standard.IDialect
	db     *sql.DB
	config standard.IConfig
}

func (c *connect) Name() string {
	return c.config.Name()
}

func (c *connect) Driver() string {
	return c.config.Driver()
}

func (c *connect) DB() *sql.DB {
	return c.db
}

func New(config standard.IConfig) standard.IConnect {
	driver := config.Driver()
	db, err := sql.Open(driver, config.DNS())
	if err != nil {
		logger.Error(err)
		return nil
	}
	db.SetConnMaxLifetime(config.MaxLifetime())
	db.SetMaxIdleConns(config.MaxIdle())
	db.SetMaxOpenConns(config.MaxOpen())
	if err = db.Ping(); err != nil {
		logger.Error(err)
		return nil
	}
	dial, ok := Init().Get(driver)
	if !ok {
		logger.Errorf("dial %s Not Found", driver)
		return nil
	}
	return &connect{config: config, db: db, dial: dial}
}

func (c *connect) Close() {
	if err := c.db.Close(); err != nil {
		logger.Error("failed to close db")
		return
	}
	logger.Info("close db success")
}
