package connect

import (
	"sync"

	"gitee.com/thoohv5/orm/standard"
)

var (
	d    *Dialect
	__od sync.Once
)

/**
本地化
*/
type Dialect struct {
	m map[string]standard.IDialect
}

// 初始化本地化
func Init() *Dialect {
	__od.Do(func() {
		d = new(Dialect)
		d.m = make(map[string]standard.IDialect, 0)
	})
	return d
}

// 装载本地化
func (d *Dialect) Register(name string, dial standard.IDialect) {
	d.m[name] = dial
}

// 获取本地化
func (d *Dialect) Get(name string) (dial standard.IDialect, ok bool) {
	dial, ok = d.m[name]
	return
}
