package mysql

import (
	"gitee.com/thoohv5/orm/connect"
	"gitee.com/thoohv5/orm/standard"
)

type dialect struct{}

var _ standard.IDialect = (*dialect)(nil)

func init() {
	connect.Init().Register("mysql", &dialect{})
}

// 是否存在表
func (s *dialect) IsExistTable(tableName string) (string, []interface{}) {
	args := []interface{}{tableName}
	return "SELECT name FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = ?", args
}
