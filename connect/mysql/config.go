package mysql

import (
	"time"

	_ "github.com/go-sql-driver/mysql"

	"gitee.com/thoohv5/orm/standard"
)

const (
	DefaultConfigName = "default"
)

/**
mysql 配置模版
*/
type Config struct {
	name        string
	dsn         string
	maxIdle     int           // 最大空闲连接数 默认值2
	maxOpen     int           // 数据库最大连接数 默认值0，无限制
	maxLifetime time.Duration // 连接最长存活期，超过这个时间连接将不再被复用 默认值0，永不过期
}

// open
func Open(dsn string) standard.IConfig {
	return &Config{
		name: DefaultConfigName,
		dsn:  dsn,
	}
}

// new
func New(config *Config) standard.IConfig {
	return config
}

func (c *Config) Name() string {
	return c.name
}

func (c *Config) Driver() string {
	return "mysql"
}

func (c *Config) DNS() string {
	return c.dsn
}

func (c *Config) MaxIdle() int {
	return c.maxIdle
}

func (c *Config) MaxOpen() int {
	return c.maxOpen
}

func (c *Config) MaxLifetime() time.Duration {
	return c.maxLifetime
}
