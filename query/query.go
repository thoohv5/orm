package query

import (
	"context"
	"database/sql"
	"errors"
	"fmt"
	"reflect"
	"strings"

	"gitee.com/thoohv5/orm/logger"
	"gitee.com/thoohv5/orm/standard"
)

type query struct {
	db      *sql.DB
	tx      *sql.Tx
	sql     strings.Builder
	sqlVars []interface{}
}

func NewQuery(db *sql.DB) standard.IQuery {
	return &query{
		db: db,
	}
}

func (s *query) Clear() {
	s.sql.Reset()
	s.sqlVars = nil
}

func (s *query) DB() standard.DB {
	if s.tx != nil {
		return s.tx
	}
	return s.db
}

func (s *query) Raw(sql string, values ...interface{}) standard.IQuery {
	s.sql.WriteString(sql)
	s.sql.WriteString(" ")
	s.sqlVars = append(s.sqlVars, values...)
	return s
}

func (s *query) Exec() (result sql.Result, err error) {
	defer s.Clear()
	s.toLog(s.sql.String(), s.sqlVars...)
	if result, err = s.DB().Exec(s.sql.String(), s.sqlVars...); err != nil {
		logger.Error(err)
	}
	return
}

// 单条查询
func (s *query) Query(ctx context.Context, dest ...interface{}) error {
	defer s.Clear()
	s.toLog(s.sql.String(), s.sqlVars...)
	return s.DB().QueryRowContext(ctx, s.sql.String(), s.sqlVars...).Scan(dest...)
}

// 单条查询
func (s *query) Find(ctx context.Context, dest interface{}) error {
	defer s.Clear()
	s.toLog(s.sql.String(), s.sqlVars...)
	// 扫描数据
	destField, err := s.scanStruct(dest)
	if nil != err {
		return err
	}
	return s.DB().QueryRowContext(ctx, s.sql.String(), s.sqlVars...).Scan(destField...)
}

// 多条查询
func (s *query) Get(ctx context.Context, dest interface{}) (err error) {
	defer s.Clear()
	s.toLog(s.sql.String(), s.sqlVars...)
	dv := reflect.ValueOf(dest)
	// 检查类型
	if dv.Kind() != reflect.Ptr {
		return fmt.Errorf("dest type err： %w", err)
	}
	if dv.IsNil() {
		return errors.New("dest type nil")
	}
	// 查询数据
	rows, err := s.DB().QueryContext(ctx, s.sql.String(), s.sqlVars...)
	if err != nil {
		return fmt.Errorf("db QueryContext err： %w", err)
	}
	defer func() {
		// 关闭rows
		if err := rows.Close(); nil != err {
			err = fmt.Errorf("rows close err: %w", err)
			return
		}
	}()
	dd := reflect.Indirect(dv)
	if dd.Kind() != reflect.Slice {
		return fmt.Errorf("dest type err： %w", err)
	}
	isPtr := false
	dt := dd.Type().Elem()
	if dt.Kind() == reflect.Ptr {
		isPtr = true
	}
	if isPtr {
		dt = dt.Elem()
	}
	for rows.Next() {
		item := reflect.New(dt).Interface()
		// 扫描数据
		df, err := s.scanStruct(item)
		if nil != err {
			return err
		}
		if err := rows.Scan(df...); nil != err {
			return err
		}
		iv := reflect.ValueOf(item)
		if !isPtr {
			iv = reflect.Indirect(iv)
		}
		dd.Set(reflect.Append(dd, iv))
	}
	return nil
}

// Begin
func (s *query) Begin(ctx context.Context) error {
	tx, err := s.db.BeginTx(ctx, nil)
	if nil != err {
		return err
	}
	s.tx = tx
	return nil
}

// Commit
func (s *query) Commit() error {
	return s.tx.Commit()
}

// Rollback
func (s *query) Rollback() error {
	return s.tx.Rollback()
}

// 扫描数据
func (s *query) scanStruct(dest interface{}) ([]interface{}, error) {
	// 检查类型
	dt := reflect.TypeOf(dest)
	if dt.Kind() != reflect.Ptr {
		return nil, fmt.Errorf("dest type err")
	}
	dvfA := make([]interface{}, 0)
	dt = dt.Elem()
	dv := reflect.Indirect(reflect.ValueOf(dest))
	for i := 0; i < dt.NumField(); i++ {
		if tag, ok := dt.Field(i).Tag.Lookup(standard.FieldTag); ok && tag == standard.IgnoreTag {
			continue
		}
		if dvf := dv.Field(i); dvf.CanAddr() {
			dvfA = append(dvfA, dvf.Addr().Interface())
		}
	}
	return dvfA, nil
}

func (s *query) toLog(query string, args ...interface{}) {
	for i := 0; i < len(args); i++ {
		query = strings.Replace(query, "?", fmt.Sprint(args[i]), 1)
	}
	logger.Infof("sql : %s", query)
}
