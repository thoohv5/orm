package query

import (
	"context"
	"database/sql"
	"fmt"
	"os"
	"testing"

	_ "github.com/go-sql-driver/mysql"

	"gitee.com/thoohv5/orm/standard"
)

var db *sql.DB

type Aa struct {
	Id   int    `db:"id"`
	Type int    `db:"type"`
	Data string `db:"data"`
}

func (b *Aa) Connection() standard.IConnect {
	return nil
}

func (b *Aa) TableName() string {
	return "aa"
}

func TestMain(m *testing.M) {

	mdb, err := sql.Open("mysql", "root:password@tcp(mysql:3306)/test?charset=utf8mb4&parseTime=true&loc=Local")
	fmt.Println(mdb, err)
	db = mdb

	os.Exit(m.Run())
}

// 执行
func Test_query_Exec(t *testing.T) {
	tests := []struct {
		name    string
		wantErr bool
	}{
		{
			"执行",
			false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := NewQuery(db)
			gotResult, err := s.Raw("INSERT INTO aa(type, data) VALUES(?, ?);", 1, 3).Exec()
			if (err != nil) != tt.wantErr {
				t.Errorf("Exec() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if nil == err {
				lastInsertId, err := gotResult.LastInsertId()
				t.Logf("%+v, %+v", lastInsertId, err)
				rowsAffected, err := gotResult.RowsAffected()
				t.Logf("%+v, %+v", rowsAffected, err)
				t.Logf("%+v", err)
			}
		})
	}
}

// 单条查询
func Test_query_Query(t *testing.T) {
	type args struct {
		ctx context.Context
	}
	data := ""
	tests := []struct {
		name string
		args args
	}{
		{
			"单条查询",
			args{ctx: context.Background()},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := NewQuery(db)
			err := s.Raw("select p.data from aa as p where p.id = ?;", 1).Query(tt.args.ctx, &data)
			t.Log(err, data)
		})
	}
}

// Find
func Test_query_Find(t *testing.T) {
	type args struct {
		ctx  context.Context
		dest interface{}
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		{
			"Find",
			args{
				ctx:  context.Background(),
				dest: new(Aa),
			},
			false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := NewQuery(db).Raw("select p.* from aa as p where p.id = ?;", 40).Find(tt.args.ctx, tt.args.dest); (err != nil) != tt.wantErr {
				t.Errorf("Find() error = %v, wantErr %v", err, tt.wantErr)
			}
			t.Log(tt.args.dest)
		})
	}
}

// Get
func Test_query_Get(t *testing.T) {
	type args struct {
		ctx  context.Context
		dest interface{}
	}
	aa := make([]*Aa, 0)
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		{
			"Get",
			args{
				ctx:  context.Background(),
				dest: &aa,
			},
			false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := NewQuery(db).Raw("select p.* from aa as p where p.id = ?;", 40).Get(tt.args.ctx, tt.args.dest); (err != nil) != tt.wantErr {
				t.Errorf("Get() error = %v, wantErr %v", err, tt.wantErr)
			}
			if len(aa) > 0 {
				t.Logf("%+v", aa[0])
			}
			t.Logf("%+v", aa)
		})
	}
}
