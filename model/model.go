package model

import (
	"context"
	"sync"

	"gitee.com/thoohv5/orm/builder"
	"gitee.com/thoohv5/orm/standard"
)

type Model struct {
	build standard.IBuilder
	model standard.IModel
}

var (
	lock    sync.RWMutex
	__mConn map[string]standard.IConnect
)

func RegisterModel(model standard.IModel) *Model {
	defer lock.RUnlock()
	lock.RLock()
	conn, ok := __mConn[model.Connection()]
	if !ok {
		panic("please register Conn")
	}
	return &Model{
		build: builder.New(conn.DB()),
		model: model,
	}
}

func InitConn(mConn map[string]standard.IConnect) {
	defer lock.Unlock()
	lock.Lock()
	__mConn = mConn
}

func RegisterConn(name string, conn standard.IConnect) {
	defer lock.Unlock()
	lock.Lock()
	__mConn[name] = conn
}

func (b *Model) GetCommonReq() *standard.CommonReq {
	return &standard.CommonReq{
		Start: 0,
		Limit: 0,
		Sorts: nil,
	}
}

// Add
func (b *Model) Add(ctx context.Context, value standard.ITable) error {
	return b.build.Table(value).WithContext(ctx).Insert(value)
}

// Update
func (b *Model) Update(ctx context.Context, param standard.IModelQuery, update map[string]interface{}) error {
	return b.Filter(b.build.Table(b.model).WithContext(ctx), param).Update(update)
}

// Delete
func (b *Model) Delete(ctx context.Context, param standard.IModelQuery) error {
	return b.Filter(b.build.Table(b.model).WithContext(ctx), param).Delete()
}

// Find
func (b *Model) Find(ctx context.Context, param standard.IModelQuery, result standard.ITable) error {
	return b.Filter(b.build.Table(b.model).WithContext(ctx), param).Find(result)
}

// Get
func (b *Model) Get(ctx context.Context, param standard.IModelQuery, result interface{}) error {
	return b.Filter(b.build.Table(b.model).WithContext(ctx), param).Get(result)
}

// GetBuilder
func (b *Model) GetBuilder() standard.IBuilder {
	return b.build
}

// SetBuilder
func (b *Model) SetBuilder(build standard.IBuilder) standard.IModel {
	b.build = build
	return b.model
}

// Filter
func (b *Model) Filter(build standard.IBuilder, param standard.IModelQuery) standard.IBuilder {
	return b.model.BuildFilterQuery(build, param).Select("*")
}
