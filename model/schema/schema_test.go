package schema

import (
	"database/sql"
	"os"
	"reflect"
	"testing"

	_ "gitee.com/thoohv5/orm/connect/dialect/mysql"
	"gitee.com/thoohv5/orm/standard"
)

type connect struct {
}

func (c *connect) Driver() string {
	return "mysql"
}
func (c *connect) DB() *sql.DB {
	return nil
}

type Aa struct {
	Id   int    `db:"id"`
	Type int    `db:"type"`
	Data string `db:"data"`
}

func (b *Aa) Connection() standard.IConnect {
	return &connect{}
}

func (b *Aa) TableName() string {
	return "aa"
}

var (
	iSchema standard.ISchema
	model   standard.IModel
)

func TestMain(m *testing.M) {

	model = new(Aa)
	iSchema = New().Parse(model)

	os.Exit(m.Run())
}

// GetTableName
func Test_schema_GetTableName(t *testing.T) {
	tests := []struct {
		name string
		want string
	}{
		{
			"GetTableName",
			"aa",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := iSchema.GetTableName(); got != tt.want {
				t.Errorf("GetTableName() = %v, want %v", got, tt.want)
			}
		})
	}
}

// GetField
func Test_schema_GetField(t *testing.T) {
	type args struct {
		name string
	}
	tests := []struct {
		name string
		args args
		want *standard.Field
	}{
		{
			"GetField",
			args{name: "Id"},
			&standard.Field{
				Name: "Id",
				Type: "int",
				Tag:  "id",
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := iSchema.GetField(tt.args.name); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GetField() = %v, want %v", got, tt.want)
			}
		})
	}
}

// GetFieldKeys
func Test_schema_GetFieldKeys(t *testing.T) {
	tests := []struct {
		name string
		want []string
	}{
		{
			"GetFieldKeys",
			[]string{"Id", "Type", "Data"},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := iSchema.GetFieldKeys(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GetFieldKeys() = %v, want %v", got, tt.want)
			}
		})
	}
}

// GetFieldValues
func Test_schema_GetFieldValues(t *testing.T) {
	tests := []struct {
		name string
		want []interface{}
	}{
		{
			"GetFieldValues",
			[]interface{}{&standard.Field{
				Name: "Id",
				Type: "int",
				Tag:  "id",
			}, &standard.Field{
				Name: "Type",
				Type: "int",
				Tag:  "type",
			}, &standard.Field{
				Name: "Data",
				Type: "string",
				Tag:  "data",
			}},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := iSchema.GetFieldValues(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GetFieldValues() = %v, want %v", got, tt.want)
			}
		})
	}
}

// Model
func Test_schema_Model(t *testing.T) {
	tests := []struct {
		name string
		want standard.IModel
	}{
		{
			"Model",
			model,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := iSchema.Model(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Model() = %v, want %v", got, tt.want)
			}
		})
	}
}

// Values
func Test_schema_Values(t *testing.T) {
	type args struct {
		d interface{}
	}
	tests := []struct {
		name string
		args args
		want []interface{}
	}{
		{
			"Values/*struct",
			args{d: Aa{
				Id:   1,
				Type: 2,
				Data: "3",
			}},
			[]interface{}{
				1, 2, "3",
			},
		},
		{
			"Values/map",
			args{d: map[string]interface{}{
				"id":   1,
				"type": 2,
				"data": "3",
			}},
			[]interface{}{
				1, 2, "3",
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := iSchema.Values(tt.args.d); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Values() = %v, want %v", got, tt.want)
			}
		})
	}
}
