package schema

import (
	"fmt"
	"go/ast"
	"reflect"

	"gitee.com/thoohv5/orm/connect"
	"gitee.com/thoohv5/orm/standard"
	"gitee.com/thoohv5/orm/util"
)

const (
	FieldTag = "db"
)

// 模型
type schema struct {
	// 模型
	model standard.IModel
	// 字段集合
	fieldMap map[string]*standard.Field
}

func New() standard.ISchema {
	return &schema{
		fieldMap: make(map[string]*standard.Field),
	}
}

/**
需要先执行Parse()
*/
func (s *schema) GetTableName() string {
	return s.model.TableName()
}

/**
需要先执行Parse()
*/
func (s *schema) GetField(name string) *standard.Field {
	return s.fieldMap[name]
}

/**
需要先执行Parse()
*/
func (s *schema) GetFieldKeys() []string {
	keys := make([]string, 0)
	for key := range s.fieldMap {
		keys = append(keys, key)
	}
	return keys
}

/**
需要先执行Parse()
*/
func (s *schema) GetFieldValues() []interface{} {
	values := make([]interface{}, 0)
	for _, value := range s.fieldMap {
		values = append(values, value)
	}
	return values
}

/**
需要先执行Parse()
*/
func (s *schema) Model() standard.IModel {
	return s.model
}

/**
解析 Model
*/
func (s *schema) Parse(m standard.IModel) standard.ISchema {
	d, ok := connect.Get(m.Connection().DriverName())
	if !ok {
		panic("not support driver type")
	}

	mType := reflect.Indirect(reflect.ValueOf(m)).Type()
	for i := 0; i < mType.NumField(); i++ {
		p := mType.Field(i)
		if !p.Anonymous && ast.IsExported(p.Name) {
			field := &standard.Field{
				Name: p.Name,
				Type: d.Parse(reflect.Indirect(reflect.New(p.Type))),
			}
			if v, ok := p.Tag.Lookup(FieldTag); ok {
				field.Tag = v
			} else {
				field.Tag = util.Lcfirst(p.Name)
			}
			s.fieldMap[p.Name] = field
		}
	}
	s.model = m
	return s
}

/**
需要先执行Parse()
*/
func (s *schema) Values(d interface{}) []interface{} {
	fmt.Println(reflect.TypeOf(d))
	var fieldValues []interface{}
	dValue := reflect.Indirect(reflect.ValueOf(d))
	switch reflect.TypeOf(d).Kind() {
	case reflect.Map:
		for _, field := range s.fieldMap {
			fieldValues = append(fieldValues, dValue.MapIndex(reflect.ValueOf(field.Tag)).Interface())
		}
	case reflect.Ptr, reflect.Struct:
		for _, field := range s.fieldMap {
			fieldValues = append(fieldValues, dValue.FieldByName(field.Name).Interface())
		}
	}
	return fieldValues
}
