package logger

import "fmt"

var defaultLog ILogger

func init() {
	defaultLog = &defaultLogger{}
}

func Info(args ...interface{}) {
	getLogger().Info(args...)
}

func Infof(format string, args ...interface{}) {
	getLogger().Infof(format, args...)
}
func Debug(args ...interface{}) {
	getLogger().Debug(args...)
}
func Debugf(format string, args ...interface{}) {
	getLogger().Debugf(format, args...)
}
func Error(args ...interface{}) {
	getLogger().Error(args...)
}
func Errorf(format string, args ...interface{}) {
	getLogger().Errorf(format, args...)
}

func SetLogger(l ILogger) {
	defaultLog = l
}

func getLogger() ILogger {
	return defaultLog
}

type defaultLogger struct {
}

func (dl *defaultLogger) Info(args ...interface{}) {
	fmt.Println(args)
}

func (dl *defaultLogger) Infof(format string, args ...interface{}) {
	fmt.Printf(fmt.Sprintf("%s\n", format), args...)
}
func (dl *defaultLogger) Debug(args ...interface{}) {
	fmt.Println(args)
}
func (dl *defaultLogger) Debugf(format string, args ...interface{}) {
	fmt.Printf(fmt.Sprintf("%s\n", format), args...)
}
func (dl *defaultLogger) Error(args ...interface{}) {
	fmt.Println(args...)
}
func (dl *defaultLogger) Errorf(format string, args ...interface{}) {
	fmt.Printf(fmt.Sprintf("%s\n", format), args...)
}
