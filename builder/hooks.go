package builder

import "gitee.com/thoohv5/orm/standard"

// Hooks constants
const (
	BeforeQuery  = "BeforeQuery"
	AfterQuery   = "AfterQuery"
	BeforeUpdate = "BeforeUpdate"
	AfterUpdate  = "AfterUpdate"
	BeforeDelete = "BeforeDelete"
	AfterDelete  = "AfterDelete"
	BeforeInsert = "BeforeInsert"
	AfterInsert  = "AfterInsert"
)

type IBeforeQuery interface {
	BeforeQuery(build standard.IBuilder)
}
type IAfterQuery interface {
	AfterQuery(build standard.IBuilder)
}
type IBeforeUpdate interface {
	BeforeUpdate(build standard.IBuilder)
}
type IAfterUpdate interface {
	AfterUpdate(build standard.IBuilder)
}
type IBeforeDelete interface {
	BeforeDelete(build standard.IBuilder)
}
type IAfterDelete interface {
	AfterDelete(build standard.IBuilder)
}
type IBeforeInsert interface {
	BeforeInsert(build standard.IBuilder)
}
type IAfterInsert interface {
	AfterInsert(build standard.IBuilder)
}

// Call calls the registered hooks
func (b *builder) Call(method string, value interface{}) {
	// fm := reflect.ValueOf(b.table).MethodByName(method)
	// if !fm.IsValid() {
	// 	if value != nil {
	// 		fm = reflect.ValueOf(value).MethodByName(method)
	// 	}
	// }
	// param := []reflect.Value{reflect.ValueOf(b)}
	// if fm.IsValid() {
	// 	if v := fm.Call(param); len(v) > 0 {
	// 		if err, ok := v[0].Interface().(error); ok {
	// 			logger.Error(err)
	// 		}
	// 	}
	// }
	// return
	switch method {
	case BeforeQuery:
		if i, ok := b.table.(IBeforeQuery); ok {
			i.BeforeQuery(b)
		}
		if i, ok := value.(IBeforeQuery); ok {
			i.BeforeQuery(b)
		}
	case AfterQuery:
		if i, ok := b.table.(IAfterQuery); ok {
			i.AfterQuery(b)
		}
		if i, ok := value.(IAfterQuery); ok {
			i.AfterQuery(b)
		}
	case BeforeUpdate:
		if i, ok := b.table.(IBeforeUpdate); ok {
			i.BeforeUpdate(b)
		}
		if i, ok := value.(IBeforeUpdate); ok {
			i.BeforeUpdate(b)
		}
	case AfterUpdate:
		if i, ok := b.table.(IAfterUpdate); ok {
			i.AfterUpdate(b)
		}
		if i, ok := value.(IAfterUpdate); ok {
			i.AfterUpdate(b)
		}
	case BeforeDelete:
		if i, ok := b.table.(IBeforeDelete); ok {
			i.BeforeDelete(b)
		}
		if i, ok := value.(IBeforeDelete); ok {
			i.BeforeDelete(b)
		}
	case AfterDelete:
		if i, ok := b.table.(IAfterDelete); ok {
			i.AfterDelete(b)
		}
		if i, ok := value.(IAfterDelete); ok {
			i.AfterDelete(b)
		}
	case BeforeInsert:
		if i, ok := b.table.(IBeforeInsert); ok {
			i.BeforeInsert(b)
		}
		if i, ok := value.(IBeforeInsert); ok {
			i.BeforeInsert(b)
		}
	case AfterInsert:
		if i, ok := b.table.(IAfterInsert); ok {
			i.AfterInsert(b)
		}
		if i, ok := value.(IAfterInsert); ok {
			i.AfterInsert(b)
		}
	}
}
