package builder

import (
	"context"
	"database/sql"
	"errors"
	"reflect"
	"strings"

	"gitee.com/thoohv5/orm/builder/grammar"
	"gitee.com/thoohv5/orm/query"
	"gitee.com/thoohv5/orm/standard"
	"gitee.com/thoohv5/orm/util"
)

// ModelQuery
type builder struct {

	/** 内部对象 **/
	// 组装
	grammar standard.IGrammar
	// 查询对象
	query standard.IQuery
	// 上下文
	ctx context.Context

	/** 外部对象 **/
	// 表
	table standard.ITable
}

func New(db *sql.DB) standard.IBuilder {
	return &builder{
		grammar: grammar.New(),
		query:   query.NewQuery(db),
	}
}

func (b *builder) WithContext(ctx context.Context) standard.IBuilder {
	b.ctx = ctx
	return b
}

func (b *builder) TableName(tableName string) standard.IBuilder {
	b.table = standard.NewDefaultTable(tableName)
	return b
}

func (b *builder) Table(table standard.ITable) standard.IBuilder {
	b.table = table
	return b
}

// receive struct/map
func (b *builder) __insert(value interface{}) ([]string, []interface{}, error) {
	keys := make([]string, 0)
	values := make([]interface{}, 0)

	vv := reflect.ValueOf(value)
	vt := reflect.TypeOf(value)
	switch vv.Kind() {
	case reflect.Ptr:
		vv = reflect.Indirect(vv)
		vt = vt.Elem()
		fallthrough
	case reflect.Struct:
		for i := 0; i < vv.NumField(); i++ {
			vtf := vt.Field(i)
			tag := vtf.Tag.Get(standard.FieldTag)
			if tag == standard.IgnoreTag {
				continue
			}
			if tag == "" {
				tag = util.Lcfirst(util.Camel2Case(vtf.Name))
			}
			if tagList := strings.Split(tag, ","); len(tagList) > 0 {
				tag = tagList[0]
			}
			keys = append(keys, tag)
			values = append(values, vv.Field(i).Interface())
		}
	case reflect.Map:
		mr := reflect.ValueOf(vv).MapRange()
		for mr.Next() {
			k := mr.Key()
			if k.Type().Kind() != reflect.String {
				return nil, nil, errors.New("insert map key err")
			}
			v := mr.Value()
			keys = append(keys, util.Lcfirst(util.Camel2Case(k.String())))
			values = append(values, v.Interface())
		}
	}
	return keys, values, nil
}

func (b *builder) Insert(value interface{}) error {
	b.Call(BeforeInsert, value)
	keys := make([]string, 0)
	values := make([]interface{}, 0)

	vv := reflect.Indirect(reflect.ValueOf(value))
	switch vv.Kind() {
	case reflect.Slice:
		for i := 0; i < vv.Len(); i++ {
			vvi := reflect.Indirect(vv.Index(i))
			k, v, err := b.__insert(vvi.Interface())
			if nil != err {
				return err
			}
			if len(keys) == 0 {
				keys = append(keys, k...)
			}
			values = append(values, v)
		}
		b.grammar.Set(grammar.VALUES, values...)
	default:
		k, v, err := b.__insert(value)
		if nil != err {
			return err
		}
		keys = k
		values = v
		b.grammar.Set(grammar.VALUES, values)
	}

	if len(b.table.TableName()) == 0 {
		return errors.New("insert table name err")
	}
	b.grammar.Set(grammar.INSERT, b.table.TableName(), keys)

	sqlStr, vars := b.grammar.Build(grammar.INSERT, grammar.VALUES)
	ret, err := b.query.Raw(sqlStr, vars...).Exec()
	if err != nil {
		return err
	}

	// todo 如果是model,赋值主键Id
	if _, ok := value.(standard.ITable); ok {
		vv := reflect.ValueOf(value).Elem()
		lastInsertId, _ := ret.LastInsertId()
		// todo 这儿应该找自增主键
		vv.FieldByName("Id").SetInt(lastInsertId)
	}
	b.Call(AfterInsert, value)
	return nil
}

// support map[string]interface{}
// also support kv list: "Driver", "Tom", "Age", 18, ....
func (b *builder) Update(attrs ...interface{}) error {
	b.Call(BeforeUpdate, attrs)
	m, ok := attrs[0].(map[string]interface{})
	if !ok {
		m = make(map[string]interface{})
		for i := 0; i < len(attrs); i += 2 {
			m[attrs[i].(string)] = attrs[i+1]
		}
	}
	b.grammar.Set(grammar.UPDATE, b.table.TableName(), m)
	sqlStr, vars := b.grammar.Build(grammar.UPDATE, grammar.WHERE)
	_, err := b.query.Raw(sqlStr, vars...).Exec()
	if err != nil {
		return err
	}
	b.Call(AfterUpdate, attrs)
	return nil
}

func (b *builder) Delete() error {
	b.Call(BeforeDelete, nil)
	b.grammar.Set(grammar.DELETE, b.table.TableName())
	sqlStr, vars := b.grammar.Build(grammar.DELETE, grammar.WHERE)
	_, err := b.query.Raw(sqlStr, vars...).Exec()
	if err != nil {
		return err
	}
	b.Call(AfterDelete, nil)
	return nil
}

func (b *builder) Select(value ...string) standard.IBuilder {
	b.grammar.Set(grammar.SELECT, b.table.TableName(), value)
	return b
}

func (b *builder) Find(value interface{}) error {
	dest := reflect.Indirect(reflect.ValueOf(value))
	destSlice := reflect.New(reflect.SliceOf(dest.Type())).Elem()
	if err := b.Limit(1).Get(destSlice.Addr().Interface()); err != nil {
		return err
	}
	if destSlice.Len() == 0 {
		return errors.New("NOT FOUND")
	}
	dest.Set(destSlice.Index(0))
	return nil
}

func (b *builder) Get(values interface{}) error {
	b.Call(BeforeQuery, values)
	sqlStr, vars := b.grammar.Build(grammar.SELECT, grammar.WHERE, grammar.ORDER, grammar.LIMIT, grammar.OFFSET)
	if err := b.query.Raw(sqlStr, vars...).Get(b.context(), values); nil != err {
		return err
	}
	b.Call(AfterQuery, values)
	return nil
}

// Count records with where clause
func (b *builder) Count(value interface{}) error {
	b.grammar.Set(grammar.COUNT, b.table.TableName())
	sqlStr, vars := b.grammar.Build(grammar.COUNT, grammar.WHERE)
	return b.query.Raw(sqlStr, vars...).Query(b.context(), &value)
}

func (b *builder) Offset(offset interface{}) standard.IBuilder {
	b.grammar.Set(grammar.OFFSET, offset)
	return b
}

func (b *builder) Limit(limit interface{}) standard.IBuilder {
	b.grammar.Set(grammar.LIMIT, limit)
	return b
}

func (b *builder) Where(query interface{}, args ...interface{}) standard.IBuilder {
	var vars []interface{}
	b.grammar.Set(grammar.WHERE, append(append(vars, query), args...)...)
	return b
}

func (b *builder) Order(value ...interface{}) standard.IBuilder {
	b.grammar.Set(grammar.ORDER, value...)
	return b
}

// 事务开始
func (b *builder) Begin(ctx context.Context) (standard.IBuilder, error) {
	return b, b.query.Begin(ctx)
}

// 事务回滚
func (b *builder) Rollback() error {
	return b.query.Rollback()
}

// 事务提交
func (b *builder) Commit() error {
	return b.query.Commit()
}

func (b *builder) context() context.Context {
	if b.ctx == nil {
		b.ctx = context.Background()
	}
	return b.ctx
}
