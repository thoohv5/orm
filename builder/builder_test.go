package builder

import (
	"database/sql"
	"fmt"
	"os"
	"testing"

	_ "github.com/go-sql-driver/mysql"

	_ "gitee.com/thoohv5/orm/connect/mysql"
	"gitee.com/thoohv5/orm/standard"
)

type connect struct {
}

func (c *connect) Name() string {
	return "default"
}

func (c *connect) Driver() string {
	return "mysql"
}
func (c *connect) DB() *sql.DB {
	return db
}

type Aa struct {
	Id   int    `db:"id"`
	Type int    `db:"type"`
	Data string `db:"data"`
}

func (b *Aa) Connection() standard.IConnect {
	return &connect{}
}

func (b *Aa) TableName() string {
	return "aa"
}

var db *sql.DB

func TestMain(m *testing.M) {

	mdb, err := sql.Open("mysql", "root:password@tcp(mysql:3306)/test?charset=utf8mb4&parseTime=true&loc=Local")
	fmt.Println(mdb, err)
	db = mdb

	os.Exit(m.Run())
}

// 插入
func Test_builder_Insert(t *testing.T) {
	type args struct {
		value interface{}
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		{
			"插入",
			args{value: &Aa{
				Type: 2,
				Data: "3",
			}},
			false,
		},
		{
			"插入",
			args{value: []Aa{
				{
					Type: 2,
					Data: "3",
				},
				{
					Type: 3,
					Data: "4",
				},
			}},
			false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			a := &Aa{}
			if err := New(db).TableName("aa").Insert(tt.args.value); (err != nil) != tt.wantErr {
				t.Errorf("Insert() error = %v, wantErr %v", err, tt.wantErr)
			}
			t.Log(a)
		})
	}
}

// Update
func Test_builder_Update(t *testing.T) {
	tests := []struct {
		name    string
		wantErr bool
	}{
		{
			"插入",
			false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := New(db).TableName("aa").Update(map[string]interface{}{
				"type": 3,
			}); (err != nil) != tt.wantErr {
				t.Errorf("Update() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

// Delete
func Test_builder_Delete(t *testing.T) {
	tests := []struct {
		name    string
		wantErr bool
	}{
		{
			"Delete",
			false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := New(db).TableName("aa").Delete(); (err != nil) != tt.wantErr {
				t.Errorf("Delete() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

// Find
func Test_builder_Find(t *testing.T) {
	type args struct {
		value interface{}
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		{
			"Find",
			args{value: &Aa{}},
			false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := New(db).TableName("aa").Select("*").Where("id = ?", 40).Find(tt.args.value); (err != nil) != tt.wantErr {
				t.Errorf("Find() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

// Get
func Test_builder_Get(t *testing.T) {
	type args struct {
		values interface{}
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		{
			"Get",
			args{values: make([]*Aa, 0)},
			false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := New(db).TableName("aa").Select("*").Where("id = ?", 40).Get(&tt.args.values); (err != nil) != tt.wantErr {
				t.Errorf("Get() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
		t.Log(tt.args.values)
	}
}

// Limit
func Test_builder_Limit(t *testing.T) {
	type args struct {
		values interface{}
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		{
			"Limit",
			args{values: make([]*Aa, 0)},
			false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := New(db).TableName("aa").Select("*").Where("id = ?", 1).Order("id DESC", "type ASC").Offset(1).Limit(2).Get(&tt.args.values); (err != nil) != tt.wantErr {
				t.Errorf("Limit() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}
