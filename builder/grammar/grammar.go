package grammar

import (
	"fmt"
	"strings"

	"gitee.com/thoohv5/orm/standard"
)

type grammar struct {
	sql          map[standard.SqlType]string
	sqlVars      map[standard.SqlType][]interface{}
	sqlVarsWhere map[string][]interface{}
}

func New() standard.IGrammar {
	return &grammar{
		sql:          make(map[standard.SqlType]string),
		sqlVars:      make(map[standard.SqlType][]interface{}),
		sqlVarsWhere: make(map[string][]interface{}),
	}
}

func (g *grammar) Set(name standard.SqlType, vars ...interface{}) {
	sql, vars := generators[name](vars...)
	if name == WHERE {
		g.sqlVarsWhere[sql] = vars
	} else {
		g.sql[name] = sql
		g.sqlVars[name] = vars
	}

}

func (g *grammar) Build(sqlVars ...standard.SqlType) (string, []interface{}) {
	var sqlList []string
	var vars []interface{}
	for _, sqlVar := range sqlVars {
		if sqlVar == WHERE {
			sqlList := make([]string, 0)
			varsList := make([]interface{}, 0)
			for sql, vars := range g.sqlVarsWhere {
				sqlList = append(sqlList, sql)
				varsList = append(varsList, vars...)
			}
			g.sql[sqlVar] = fmt.Sprintf("WHERE %s", strings.Join(sqlList, " AND "))
			g.sqlVars[sqlVar] = varsList
			g.sqlVarsWhere = make(map[string][]interface{}, 0)
		}
		if sql, ok := g.sql[sqlVar]; ok {
			sqlList = append(sqlList, sql)
			vars = append(vars, g.sqlVars[sqlVar]...)
		}
	}
	return strings.Join(sqlList, " "), vars
}

func genBindVars(num int) string {
	var vars []string
	for i := 0; i < num; i++ {
		vars = append(vars, "?")
	}
	return strings.Join(vars, ", ")
}

func __insert(values ...interface{}) (string, []interface{}) {
	// INSERT INTO $tableName ($fields)
	tableName := values[0]
	fields := strings.Join(values[1].([]string), ",")
	return fmt.Sprintf("INSERT INTO %s (%v)", tableName, fields), []interface{}{}
}

func __values(values ...interface{}) (string, []interface{}) {
	// VALUES ($v1), ($v2), ...
	var bindStr string
	var sql strings.Builder
	var vars []interface{}
	sql.WriteString("VALUES ")
	for i, value := range values {
		v := value.([]interface{})
		if bindStr == "" {
			bindStr = genBindVars(len(v))
		}
		sql.WriteString(fmt.Sprintf("(%v)", bindStr))
		if i+1 != len(values) {
			sql.WriteString(", ")
		}
		vars = append(vars, v...)
	}
	return sql.String(), vars

}

func __select(values ...interface{}) (string, []interface{}) {
	// SELECT $fields FROM $tableName
	tableName := values[0]
	fields := strings.Join(values[1].([]string), ",")
	return fmt.Sprintf("SELECT %v FROM %s", fields, tableName), []interface{}{}
}

func __offset(values ...interface{}) (string, []interface{}) {
	// OFFSET $num
	return "OFFSET ?", values
}

func __limit(values ...interface{}) (string, []interface{}) {
	// LIMIT $num
	return "LIMIT ?", values
}

func __where(values ...interface{}) (string, []interface{}) {
	// WHERE $desc
	desc, vars := values[0], values[1:]
	return fmt.Sprintf("%s", desc), vars
	// return fmt.Sprintf("WHERE %s", desc), vars
}

func __order(values ...interface{}) (string, []interface{}) {
	return fmt.Sprintf("ORDER BY %s", genBindVars(len(values))), values
}

func __update(values ...interface{}) (string, []interface{}) {
	tableName := values[0]
	m := values[1].(map[string]interface{})
	var keys []string
	var vars []interface{}
	for k, v := range m {
		keys = append(keys, k+" = ?")
		vars = append(vars, v)
	}
	return fmt.Sprintf("UPDATE %s SET %s", tableName, strings.Join(keys, ", ")), vars
}

func __delete(values ...interface{}) (string, []interface{}) {
	return fmt.Sprintf("DELETE FROM %s", values[0]), []interface{}{}
}

func __count(values ...interface{}) (string, []interface{}) {
	return __select(values[0], []string{"COUNT(*)"})
}
