package grammar

import "gitee.com/thoohv5/orm/standard"

type compile func(values ...interface{}) (string, []interface{})

const (
	INSERT standard.SqlType = iota
	VALUES
	SELECT
	OFFSET
	LIMIT
	WHERE
	ORDER
	UPDATE
	DELETE
	COUNT
)

var generators map[standard.SqlType]compile

func init() {
	generators = make(map[standard.SqlType]compile)
	generators[INSERT] = __insert
	generators[VALUES] = __values
	generators[SELECT] = __select
	generators[OFFSET] = __offset
	generators[LIMIT] = __limit
	generators[WHERE] = __where
	generators[ORDER] = __order
	generators[UPDATE] = __update
	generators[DELETE] = __delete
	generators[COUNT] = __count
}
