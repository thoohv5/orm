package builder

import (
	"context"
	"errors"
	"fmt"
	"runtime/debug"

	"gitee.com/thoohv5/orm/standard"
)

type Task func(ctx context.Context, build standard.IBuilder) error

func errHandler(ctx context.Context, build standard.IBuilder, task Task) (err error) {
	defer func() {
		if e := recover(); e != nil {
			msg := fmt.Sprintf("panic: %s\ncalltrace : %s", fmt.Sprint(e), string(debug.Stack()))
			err = errors.New(msg)
		}
	}()
	return task(ctx, build)
}

func ExecTrans(ctx context.Context, build standard.IBuilder, trans ...Task) error {
	db, err := build.Begin(ctx)
	if err != nil {
		return err
	}
	for _, task := range trans {
		if err := errHandler(ctx, db, task); err != nil {
			if err := db.Rollback(); err != nil {
				return err
			}
			return err
		}
	}

	if err := db.Commit(); err != nil {
		_ = db.Rollback()
		return err
	}
	return nil
}
